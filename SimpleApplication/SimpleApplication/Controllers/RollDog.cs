﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SimpleApplication.Models;

namespace SimpleApplication.Controllers
{
    public class RollDog : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public RollDog(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [EnableCors("MyPolicy")]
        public IActionResult GetStatus()
        {
            var usersPerSec = new Random().Next(0, 200) + 800;
            var errorCount = 10;
            var cpuUtilization = usersPerSec/20 + (new Random().Next(0, 100) / 100);
            var memUtilization = usersPerSec/60 + (new Random().Next(0,100)/100);
            var resObject = new { UsersPerSec = usersPerSec, ErrorCount = errorCount, MemUtilization = memUtilization, CpuUtilization = cpuUtilization };
            return new OkObjectResult(resObject);
        }
    }
}
