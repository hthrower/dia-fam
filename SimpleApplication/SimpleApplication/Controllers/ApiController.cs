﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SimpleApplication.Models;

namespace SimpleApplication.Controllers
{
    public class ApiController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public ApiController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [EnableCors("MyPolicy")]
        public IActionResult Ready()
        {
            //return new OkObjectResult( new { TestField = "field" } );
            return new StatusCodeResult(200);
        }
    }
}
