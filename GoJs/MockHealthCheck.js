class MockHealthCheck
{
    constructor(appName){
        this.appName = appName
    }
    GetMetricsFuncion = function (){ return new Promise((resolve, reject) => {
          
            resolve({
                AppName: this.appName,
                Errors: Math.floor(Math.random() * 10),
                Cpu: Math.random().toFixed(3) + "%",
                Mem: Math.random().toFixed(3) + "%"
            });
        })
    }

    GetHealthStatusFunc = function () {return new Promise((resolve, reject) => {
            resolve(true);
        })

    }

    GetHealthCheckComponent = function (){
        return new ApiReaderComponent(this.GetHealthStatusFunc, this.GetMetricsFuncion);
    }

}