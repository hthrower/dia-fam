class ApiReaderComponent{
    constructor(healthStatusFunc, getMetricsFunc, mappingFunc){
        this.healthStatusFunc = healthStatusFunc
        this.setMetricsFunction(getMetricsFunc)
        this.setMappingFunction(mappingFunc)
    }
    //function to Map values of the response to objects to be displayed in the UI
    setHealthStatusFunction(mappingFunc){
        this.healthStatusFunc = healthStatusFunc
    }

    setMappingFunction(mappingFunc){
        this.mappingFunc = mappingFunc
    }

    //Function to call, expects a Json of results to be returned
    setMetricsFunction(getMetricsFunc){
        this.getMetricsFunc = getMetricsFunc
    }

    async getMetrics(){
        var res = await this.getMetricsFunc();
        if(this.mappingFunc == null)
            return res;
        
        var statusObj = this.mappingFunc(res);
        return statusObj
    }

    async getHealthStatus(){
        return await this.healthStatusFunc()
    }

}
