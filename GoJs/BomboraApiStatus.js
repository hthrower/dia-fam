class SpyglassHealthCheck
{
  SpyglassMappingFunction = function (json){
    var result = {
      UsersPerSec: json['usersPerSec'],
      ErrorCount: json['errorCount'],
      MemUtilization: json['memUtilization'],
      CpuUtilization: json['cpuUtilization']
    }
    return result;
  }

  SpyglassGetMetricsFunction = function (){ return new Promise((resolve, reject) => {
        fetch('https://localhost:5001/RollDog/getStatus')
        .then(response => {
          resolve(response.json());
        }, response => {
          resolve(false);
        })
        .catch(false);
      })
  }

  SpyglassHealthStatusFunc = function (){
    return new Promise((resolve, reject) => {
      fetch('https://localhost:5001/api/ready')
      .then(response => {
        resolve(response.ok);
      }, response => {
        resolve(false);
      })
      .catch(false);
    });
  }

  GetHealthCheckComponent = function (){
    return new ApiReaderComponent(this.SpyglassHealthStatusFunc, this.SpyglassGetMetricsFunction,this.SpyglassMappingFunction);
  }
}