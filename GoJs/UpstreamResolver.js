class UpstreamResolver
{
	constructor(ToFromDict){
		this.ToFromDict = ToFromDict
	}

	GetUpStreamDependencies(nodeName){
      var deps = []
      deps = this.GetUpStreamHelper(nodeName, this.ToFromDict, deps)
      return new Set(deps)
    }

    GetUpStreamHelper(nodeName, nodes, deps){
      deps = deps.concat(nodes[nodeName])
      for(var i in nodes[nodeName]){
        deps = deps.concat(this.GetUpStreamHelper(nodes[nodeName][i], nodes, deps))
      }
      return deps;
    }


}